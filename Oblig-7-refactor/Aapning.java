public class Aapning extends HvitRute {
    public Aapning(int x, int y, Labyrint labyrint){
        super(x,y, labyrint);
        labyrint.antÅpninger++;
    }

    @Override
    void gå(){
        sjekket.add(this);
        labyrint.utveier.add(backTrack("") + "\n");
        if(labyrint.antÅpninger > labyrint.utveier.size()){
            if(skalSjekkes.size() != 0) skalSjekkes.poll().gå();
        }
    }
}