import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import java.io.File;
import java.io.FileNotFoundException;

public class Gui extends Application {
    Labyrint labyrint;
    Text infoText;
    int utveiNr;
    boolean ruteSelected = false;
    private int height, width;
    private Rectangle[][] rutenett;

    public static void main(String[] args) {
        launch();
    }

    private static boolean[][] losningStringTilTabell(String losningString, int bredde, int hoyde) {
        boolean[][] losning = new boolean[hoyde][bredde];
        java.util.regex.Pattern p = java.util.regex.Pattern.compile("\\(([0-9]+),([0-9]+)\\)");
        java.util.regex.Matcher m = p.matcher(losningString.replaceAll("\\s",""));
        while (m.find()) {
            int x = Integer.parseInt(m.group(1));
            int y = Integer.parseInt(m.group(2));
            losning[y][x] = true;
        }
        return losning;
    }

    @Override
    public void start(Stage stage){
        // lager en VBox til å ha alt i.
        VBox vBox = new VBox();
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(10);

        // Ber bruker om labyrint-fil.
        FileChooser fc = new FileChooser();
        fc.setTitle("Velg en labyrint fil");
        File fil = fc.showOpenDialog(stage);

        // Lager labyrint fra filen.
        try {
            labyrint = Labyrint.lesFraFil(fil);
            height = labyrint.ruter.length;
            width = labyrint.ruter[0].length;
            rutenett = new Rectangle[width][height];
        } catch (FileNotFoundException ex){
            ex.printStackTrace();
        } catch (Exception ex){
            System.out.println("Ikke en gyldig labyrint fil, avslutter.");
            System.exit(1);
        }

        // Lager rutenett med rektangler som viser labyrinten.
        GridPane gridPane = lagGridPane(500, 500);

        // Lager info-text
        infoText = new Text("Trykk på en hvit rute!");
        infoText.setFont(new Font(15));

        // Lager forrige og neste knapper.
        HBox hBox = lagHBox();

        // Lager en "Ny labyrint" knapp.
        Button nyFilKnapp = new Button("Ny labyrint");
        nyFilKnapp.setOnAction(klikk -> nyFilKnapp(stage));

        // Legger alle elementene til i vBox, og justerer padding.
        vBox.getChildren().addAll(gridPane, infoText, hBox, nyFilKnapp);
        vBox.setPadding(new Insets(0,0,10,0));

        // Oppretter Scene, og setter stage sin scene til denne.
        Scene scene = new Scene(vBox);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.setTitle("Labyrint Solver");

        // Viser vinduet.
        stage.show();
    }

    private GridPane lagGridPane(int gridPaneHøyde, int gridPaneBredde){
        // Lager et GridPane fylt med Rectangle objekter, som representerer valgt labyrint.
        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 10, 0, 10));
        for(int i = 0; i < height; i++){
            for (int j = 0; j < width; j++){
                Color color = labyrint.ruter[i][j].farge();
                Rectangle rect = new Rectangle(gridPaneBredde/width, gridPaneHøyde/height);
                rect.setFill(color);
                rect.setOnMouseClicked(new RuteClick(rect, i, j, this));
                gridPane.add(rect, j, i);
                rutenett[j][i] = rect;
            }
        }
        return gridPane;
    }

    /*void (){
        launch();
    }*/

    private HBox lagHBox(){
        HBox hBox = new HBox();

        // Bruker lambda uttrykk, fordi det sparer mye plass.
        Button forrige = new Button("Forrige");
        forrige.setOnAction(klikk -> forrigeKnapp());

        Button neste = new Button("Neste");
        neste.setOnAction(klikk -> nesteKnapp());

        hBox.getChildren().addAll(forrige, neste);
        hBox.setAlignment(Pos.CENTER);
        hBox.setPadding(new Insets(0,0,0,0));
        hBox.setSpacing(10);

        return hBox;
    }

    private void nesteKnapp(){
        // Viser neste utvei.
        if(!ruteSelected) infoText.setText("Trykk på en rute først!");
        else {
            if(utveiNr >= labyrint.utveier.size() - 1) {
                utveiNr = 0;
            } else {
                utveiNr++;
            }
            infoText.setText("Utvei " + (utveiNr+1) + "/" + labyrint.utveier.size());
            tegnUtvei();
        }

    }

    private void forrigeKnapp(){
        // Viser forrige utvei.
        if(!ruteSelected) infoText.setText("Trykk på en rute først!");
        else {
            if(utveiNr < 1){
                utveiNr = labyrint.utveier.size() - 1;
            } else {
                utveiNr--;
            }

            infoText.setText("Utvei " + (utveiNr+1) + "/" + labyrint.utveier.size());
            tegnUtvei();
        }
    }

    private void nyFilKnapp(Stage stage){
        // Lukker vinduet
        stage.close();
        // sørger for at programmet starter på nytt, med samme Stage som ble gitt fra JVM.
        Platform.runLater(() -> start(stage));
    }

    private void reset(){
        // setter alle rutene til deres opprinnelige farge.
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                rutenett[j][i].setFill(labyrint.ruter[i][j].farge());
            }
        }
    }

    void tegnUtvei(){
        reset();
        if(labyrint.utveier.size() > 0) {
            String utvei = labyrint.utveier.get(utveiNr);

            // Gjør alle ruter, som er en del av utveien, røde.
            boolean[][] losningsTabell = losningStringTilTabell(utvei, width, height);
            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    if (losningsTabell[i][j]) {
                        rutenett[j][i].setFill(Color.RED);
                    }
                }
            }
        } else {
            infoText.setText("Fant ingen utveier");
        }
    }

    class RuteClick implements EventHandler<MouseEvent>{
        // klasse som håndterer klikk på ruter.
        Rectangle rect;
        int rad, kolonne;
        Gui gui;

        RuteClick(Rectangle rect, int rad, int kolonne, Gui gui){
            this.rect = rect;
            this.rad = rad;
            this.kolonne = kolonne;
            this.gui = gui;
        }

        @Override
        public void handle(MouseEvent mouseEvent) {
            reset();
            System.out.printf("Trykket på rute (%d, %d) \n", rad, kolonne);
            labyrint.ruter[rad][kolonne].onClick(gui);
        }
    }
}