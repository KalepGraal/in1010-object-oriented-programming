import javafx.scene.paint.Color;

import java.util.*;

abstract class Rute implements Comparable<Rute>{
    static PriorityQueue<Rute> skalSjekkes;
    static Collection<Rute> sjekket;
    List<Rute> naboer; // 0N, 1S, 2W, 3E - null = tom
    int x, y;
    Rute forrige;
    Labyrint labyrint;
    double distStart;
    double distEnd;

    public Rute(int rad, int kolonne, Labyrint labyrint) {
        x = kolonne;
        y = rad;
        this.labyrint = labyrint;
    }

    abstract char tilTegn();
    abstract Color farge();
    abstract void onClick(Gui gui);
    abstract void gå();


    void finnUtvei(){
        skalSjekkes = new PriorityQueue<>();
        sjekket = new HashSet<>();

        resetAll();
        gå();
    }

    private String koordinater(){
        return "(" + x + ", " + y + ")";
    }

    String backTrack(String historikk) {
        Rute currentRute = this;
        if (currentRute.forrige == null){
            return currentRute.koordinater() + historikk;
        } else {
            historikk = " --> " + currentRute.koordinater() + historikk;
        }
        return forrige.backTrack(historikk);
    }

    @Override
    public int compareTo(Rute r) {
        int compared = Double.compare(fScore(), r.fScore());
        if(compared == 0){
            compared = Double.compare(distEnd, r.distEnd);
        }
        return compared;
    }

    private double fScore(){
        return distStart + distEnd;
    }

    private void resetAll(){
        //skalSjekkes = new PriorityQueue<>();
        for(int x = 0; x < labyrint.rader; x++) {
            for (int y = 0; y < labyrint.kolonner; y++) {
                labyrint.ruter[x][y].forrige = null;
            }
        }
    }

    double avstandTil(Rute til){
        int x = til.x - this.x;
        int y = til.y - this.y;
        // Sqrt(x^2+y^2),  lengden av en vektor
        return Math.sqrt(Math.pow(x,2)+Math.pow(y,2));
    }

    double avstandTilNærmesteUtvei(){
        Rute nærmest = labyrint.utveiRuter.get(0);
        double avstand = avstandTil(nærmest);
        for(int i = 1; i < labyrint.utveiRuter.size(); i++){
            double x = avstandTil(labyrint.utveiRuter.get(i));
            if(avstand > x){
                avstand = x;
            }
        }
        return avstand;
    }
}
