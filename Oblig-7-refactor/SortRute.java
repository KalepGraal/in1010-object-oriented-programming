import javafx.scene.paint.Color;

class SortRute extends Rute {

    public SortRute(int x, int y, Labyrint labyrint){
        super(x,y, labyrint);
    }

    @Override
    char tilTegn() {return '#';}

    @Override
    Color farge() {return Color.BLACK;}

    @Override
    void onClick(Gui gui){
        gui.ruteSelected = false;
        gui.infoText.setText("Trykk på en HVIT rute!");
    }

    @Override
    void gå(){
        // Kjører aldri, ettersom gå kun kjøres på hvite ruter
    }
}