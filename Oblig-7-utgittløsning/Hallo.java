import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Hallo extends Application{
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage teater){
        Text text = new Text("Hallo!");
        text.setFont(new Font(40));
        text.setY(40);

        Pane pane = new Pane();
        pane.getChildren().add(text);

        Scene scene = new Scene(pane);
        teater.setTitle("Halloz!");
        teater.setScene(scene);
        teater.show();
    }

}