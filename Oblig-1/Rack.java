import java.util.ArrayList;

/**
*   Klassen Rack holder styr på en ArrayListe med Noder, og har en variabel
*   som holder styr på hvor mange noder der er plass til i dette Rack objektet.
*   Konstruktøren tar kun en parameter for maks antallet Noder per Rack.
*   Noder kan legges til i Rack objektet, ved addNode, som tar et Node objekt
*   som parameter. Brukes en vanlig get-metode for å hente ut listen med noder.
*/

public class Rack {
    private ArrayList<Node> nodes;
    private int maxNodesPerRack;

    public Rack(int NodesPerRack){
        nodes = new ArrayList<Node>();
        maxNodesPerRack = NodesPerRack;
    }

    public boolean isFull(){
        return nodes.size() == maxNodesPerRack;
    }

    public void addNode(Node n){
        // Dette sjekkes også i Regneklynge, men velger å sjekke
        // her også, for å holde klassen mer selvstendig.
        if (!isFull()) {
            nodes.add(n);
        }
    }

    public ArrayList<Node> getNodes(){
        return nodes;
    }
}
