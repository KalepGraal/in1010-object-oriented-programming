import java.io.*;
import java.util.ArrayList;

/**
*   Klassen Regneklynge holder styr på en ArrayListe med Racks, og har en variabel
*   som holder styr på hvor mange noder der er plass til i Rack objektene.
*   Konstruktøren tar en filepath som parameter (String).
*/

public class Regneklynge {
    private ArrayList<Rack> racks = new ArrayList<>();
    private int maxNodesPerRack;

    public Regneklynge(String file){
        // Lager en ny regneklynge fra en fil.
        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            maxNodesPerRack = Integer.parseInt(bufferedReader.readLine());

            addNodesFromFile(bufferedReader);

            bufferedReader.close();
        }
        catch (IOException ex) {
            System.out.println("IOException: " + ex);
        }
    }

    public void addNodesFromFile(BufferedReader bufferedReader)
                throws IOException {
        // Legger til noder fra en fil.

        String line;
        while((line = bufferedReader.readLine()) != null){
            String[] liste = line.split(" ");
            int antNoder = Integer.parseInt(liste[0]);
            int minne = Integer.parseInt(liste[1]);
            int prosessorer = Integer.parseInt(liste[2]);
            addNodeToARack(antNoder, minne, prosessorer);
        }
    }

    public void addNodeToARack(int antNoder, int minne, int prosessorer){
        // Legger noder inn i et Rack, lager nytt Rack dersom det trengs.
        for (int i = 0; i < antNoder; i++){
            if (racks.size() == 0 || racks.get(racks.size() - 1).isFull()) {
            racks.add(new Rack(maxNodesPerRack));
            }
            racks.get(racks.size() - 1).addNode(new Node(minne, prosessorer));
        }
    }

    public int getTotalProcessorCount(){
        // Returnerer det totale antallet prosessorer i alle Racks.
        int processorCount = 0;

        for (Rack rack : racks) {  // for rack in racks
            ArrayList<Node> nodes = rack.getNodes();
            for (Node node : nodes){   // for node in nodes
                processorCount += node.getNumberOfProcessors();
            }
        }

        return processorCount;
    }

    public int getTotalNodesWithEnoughMemory(int requiredMemory){
        // Returnerer antallet noder i alle racks med nok minne.
        // Tar mengden minne (int) som parameter.
        int nodesWithEnoughMemory = 0;
        for (int i = 0; i < racks.size(); i++){  // for rack in racks
            ArrayList<Node> nodes = racks.get(i).getNodes();
            for (int j = 0; j < nodes.size(); j++){  // for node in nodes
                if (nodes.get(j).getMemory() >= requiredMemory){
                    nodesWithEnoughMemory++;
                }
            }
        }
        return nodesWithEnoughMemory;
    }

    public int getRackCount(){
        // returnerer antallet racks.
        return racks.size();
    }
}
