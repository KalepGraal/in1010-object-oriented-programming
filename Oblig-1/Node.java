/**
*   Klassen Node holder to variabler, en for minne,
*   og en for antallet prosessorer. Kontstruktøren tar (int) minne,
*   (int) antallet prossesorer som parametre.
*   Klassen har get-metoder for begge disse variablene.
*   (getNumberOfProcessors og getMemory)
*/

public class Node {

    private int memory;
    private int numberOfProcessors;

    public Node(int memory, int numberOfProcessors){
        this.memory = memory;
        this.numberOfProcessors = numberOfProcessors;
    }

    public int getNumberOfProcessors(){
        return numberOfProcessors;
    }

    public int getMemory(){
        return memory;
    }
}
