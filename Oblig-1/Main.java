/**
*   Klassen Main brukes for å kjøre hovedprogrammet.
*   Klassen oppretter en regneklynge fra en fil, og
*   printer så ut info om regneklyngen.
*/

public class Main {
    public static void main(String[] args) {
        String filNavn = "regneklynge.txt";

        Regneklynge abel = new Regneklynge(filNavn);

        System.out.println("Nodes with atleast 32 GB: " + abel.getTotalNodesWithEnoughMemory(32));
        System.out.println("Nodes with atleast 64 GB: " + abel.getTotalNodesWithEnoughMemory(64));
        System.out.println("Nodes with atleast 128 GB: " + abel.getTotalNodesWithEnoughMemory(128));

        System.out.println("\nTotal processors: " + abel.getTotalProcessorCount());
        System.out.println("Racks: " + abel.getRackCount());
    }
}
