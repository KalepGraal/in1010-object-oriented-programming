import java.io.File;
import java.io.FileNotFoundException;

public class Speedtest {
    public static void main(String[] args) {
        Labyrint labyrint;
        try {
            labyrint = Labyrint.lesFraFil(new File("labyrinter/enkel4.in"));

            for (int i = 0; i < 10; i++) {
                Long startTime = System.currentTimeMillis();
                labyrint.finnUtveiFra(1,1);
                System.out.println("Runtime: " + (System.currentTimeMillis() - startTime) + "ms");
            }


        } catch (FileNotFoundException ex){
            System.out.println("Fant ikke fila, avslutter");
            System.exit(1);
        }

    }
}