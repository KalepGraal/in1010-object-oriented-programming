public class SortertLenkeliste<T extends Comparable<T>> extends Lenkeliste<T>{

    @Override
    public void leggTil(T x){
        int i = 0;
        while(i < stoerrelse() && x.compareTo(hent(i)) >= 0){
            i++;
        }
        super.leggTil(i, x);
    }

    @Override
    public T fjern() {
        if (stoerrelse() == 1) return super.fjern();
        return fjern(stoerrelse()-1);
    }

    @Override
    public void sett(int pos, T x) {
        throw new UnsupportedOperationException("Kan ikke endre et element, i en sortert liste.");
    }

    @Override
    public void leggTil(int pos, T x){
        throw new UnsupportedOperationException("Kan ikke sette inn på gitt plass, i en sortert liste.");
    }
}