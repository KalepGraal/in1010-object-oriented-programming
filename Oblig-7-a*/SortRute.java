class SortRute extends Rute {

    public SortRute(int x, int y, Labyrint labyrint){
        super(x,y, labyrint);
        setTegn("\u001B[30m██\u001B[0m");
    }

    @Override
    char tilTegn() {return '#';}

    @Override
    public String toString() {return tegn;}

    @Override
    void reset(){
        closed.remove(this);
        setTegn("\u001B[30m██\u001B[0m");
    }
}