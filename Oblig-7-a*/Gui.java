import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Box;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.EventListener;
import java.util.Random;

public class Gui extends Application {
    static int height, width;
    static Labyrint labyrint;
    static Rectangle[][] rutenett;
    Stage stage;
    Text text;
    int utveiNr;
    boolean ruteSelected = false;

    static boolean[][] losningStringTilTabell(String losningString, int bredde, int hoyde) {
        boolean[][] losning = new boolean[hoyde][bredde];
        java.util.regex.Pattern p = java.util.regex.Pattern.compile("\\(([0-9]+),([0-9]+)\\)");
        java.util.regex.Matcher m = p.matcher(losningString.replaceAll("\\s",""));
        while (m.find()) {
            int x = Integer.parseInt(m.group(1));
            int y = Integer.parseInt(m.group(2));
            losning[y][x] = true;
        }
        return losning;
    }

    @Override
    public void start(Stage stage){
        this.stage = stage;
        int sWidth = 500;
        int sHeight = 500;
        // Pane pane = new Pane();
        VBox pane = new VBox();
        pane.setAlignment(Pos.CENTER);
        pane.setSpacing(10);

        FileChooser fc = new FileChooser();
        fc.setTitle("Velg en labyrint fil");
        // todo exception-handling hvis fil ikke velges; InputMismatchException, RuntimeException
        File fil = fc.showOpenDialog(stage);
        //File fil = new File("labyrinter/4.in");
        try {
            labyrint = Labyrint.lesFraFil(fil);
            height = labyrint.ruter.length;
            width = labyrint.ruter[0].length;
            rutenett = new Rectangle[width][height];
        } catch (FileNotFoundException ex){
            ex.printStackTrace();
        }


        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 10, 0, 10));

        System.out.println(width);
        System.out.println(height);
        System.out.println(rutenett.length);

        for(int i = 0; i < height; i++){ // rad
            for (int j = 0; j < width; j++){ // kolonne
                Color color;
                if(labyrint.ruter[i][j].tilTegn() == '#'){
                    color = Color.BLACK;
                } else {
                    color = Color.WHITE;
                }
                // todo dynamisk size
                Rectangle rect = new Rectangle(sWidth/width,sHeight/height);
                rect.setOnMouseClicked(new RuteClick(rect, i, j));
                rect.setFill(color);
                //rect.setStroke(Color.BLACK);
                //rect.setStrokeWidth(1);
                gridPane.add(rect, j, i);
                rutenett[j][i] = rect;
            }
        }

        text = new Text("Trykk på en hvit rute!");
        text.setFont(new Font(15));

        HBox hBox = new HBox();
        Button forrige = new Button("Forrige");
        forrige.setOnAction(new forrigeKnappBehandler());

        Button neste = new Button("Neste");
        neste.setOnAction(new nesteKnappBehandler());

        hBox.getChildren().addAll(forrige, neste);
        hBox.setAlignment(Pos.CENTER);
        hBox.setPadding(new Insets(0,0,10,0));
        hBox.setSpacing(10);


        pane.getChildren().addAll(gridPane, text, hBox);



        /*pane.getChildren().add(new Rectangle(500, 500));*/
        Scene scene = new Scene(pane);

        stage.setScene(scene);
        stage.setResizable(false);

        stage.setTitle("GUI");
        stage.show();
    }

    void test(){
        launch();
    }

    void nesteKnapp(){
        if(!ruteSelected) text.setText("Trykk på en rute først!");
        else {
            if(utveiNr == labyrint.utveier.stoerrelse() - 1) {
                utveiNr = 0;
            } else {
                utveiNr++;
            }
            text.setText("Utvei " + (utveiNr+1) + "/" + labyrint.utveier.stoerrelse());
            tegnUtvei(utveiNr);
        }

    }

    void forrigeKnapp(){
        if(!ruteSelected) text.setText("Trykk på en rute først!");
        else {
            if(utveiNr == 0){
                utveiNr = labyrint.utveier.stoerrelse() - 1;
            } else {
                utveiNr--;
            }

            text.setText("Utvei " + (utveiNr+1) + "/" + labyrint.utveier.stoerrelse());
            tegnUtvei(utveiNr);
        }
    }

    void reset(){
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if(labyrint.ruter[i][j].tilTegn() == '#'){
                    rutenett[j][i].setFill(Color.BLACK);
                } else {
                    rutenett[j][i].setFill(Color.WHITE);
                }
            }
        }
    }

    public void tegnUtvei(int pos){
        reset();
        String utvei = labyrint.utveier.hent(pos);

        boolean[][] losningsTabell = losningStringTilTabell(utvei, width, height);
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if(losningsTabell[i][j]){
                    rutenett[j][i].setFill(Color.RED);
                }
            }
        }
    }

    class RuteClick implements EventHandler<MouseEvent>{
        Rectangle rect;
        int rad, kolonne;
        RuteClick(Rectangle rect, int rad, int kolonne){
            this.rect = rect;
            this.rad = rad;
            this.kolonne = kolonne;
        }
        @Override
        public void handle(MouseEvent mouseEvent) {
            reset();
            rect.setFill(Color.BLUE);
            ruteSelected = true;
            System.out.println("Trykket på rute (" + rad + ", " + kolonne + ")");
            if(labyrint.ruter[rad][kolonne].tilTegn() == '#'){
                text.setText("Trykk på en HVIT rute!");
            } else {
                labyrint.finnUtveiFra(kolonne, rad);
                System.out.println("Fant " + labyrint.utveier.stoerrelse() + " utveier");
                text.setText("Utvei " + (utveiNr+1) + "/" + labyrint.utveier.stoerrelse());
                tegnUtvei(utveiNr);
            }
        }
    }

    class forrigeKnappBehandler implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent event) {
            forrigeKnapp();
        }
    }

    class nesteKnappBehandler implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent event) {
            nesteKnapp();
        }
    }

}