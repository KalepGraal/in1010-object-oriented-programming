import java.util.HashSet;
import java.util.LinkedList;

abstract class Rute {
    static HashSet<Rute> closed = new HashSet<>();
    private static Rute start;
    private static LinkedList<Rute> open = new LinkedList<>();
    Rute[] naboer; // 0N, 1S, 2W, 3E - null = tom
    String tegn;
    Boolean utvei = false;
    private int x, y;
    private Rute forrige;
    private Labyrint labyrint;
    private double distStart;
    private double distEnd;
    static int antAapninger;

    public Rute(int rad, int kolonne, Labyrint labyrint) {
        x = kolonne;
        y = rad;
        this.labyrint = labyrint;
    }

    abstract char tilTegn();
    abstract void reset();

    public void setTegn(String ny) {tegn = ny;}

    @Override
    abstract public String toString();

    private void gå(Rute current){
        // Mitt forsøk på å implementere A* algoritmen.
        // Ikke helt optimal, men finner en kort vei, om ikke den korteste.
        // Kjør "java Oblig5 <filnavn> detaljert" for utskrift

        // todo implementer gå sprett utover i subklassene
        // todo skriv README.txt som forklarer algoritmen, og hvilken konkurranse
        

        open.remove(current);
        closed.add(current);

        current.setTegn("\u001B[31m██\u001B[0m");
        if(labyrint.detaljert) {
            labyrint.skrivUt();
            System.out.println("Current: " + current.koordinater());
            try{ Thread.sleep(50);} catch(Exception ex){ex.printStackTrace();}
        }
        current.setTegn("\u001B[34m██\u001B[0m");

        if(current.utvei) {
            current.setTegn("\u001B[32m██\u001B[0m");
            labyrint.utveier.leggTil(current.backTrack("") + "\n");
            if(labyrint.utveier.stoerrelse() == antAapninger) return;
        } else {
            for(Rute nabo : current.naboer){
                if(nabo != null){
                    if(nabo.tilTegn() == '#' || closed.contains(nabo)) {
                        continue;
                    }
                    if(labyrint.detaljert){
                        System.out.println("DistEnd: " + current.distEnd);
                        System.out.println("distStart: " + current.distStart);
                        System.out.println("fScore: " + current.fScore());
                    }

                    double nyAvstandTilNabo = current.distStart + avstandTil(nabo);
                    if (nyAvstandTilNabo < nabo.distStart || !open.contains(nabo)){
                        nabo.forrige = current;
                        nabo.distStart = nyAvstandTilNabo;
                        nabo.distEnd = nabo.avstandTilNærmesteUtvei();
                        if (!open.contains(nabo)){
                            open.add(nabo);
                        }
                    }
                }
                if(nabo.tilTegn() == '#') nabo.setTegn("\u001B[30m██\u001B[0m");
                else nabo.setTegn("\u001B[37m██\u001B[0m");
            }
        }
        if (labyrint.detaljert) System.out.println("Open size: " + open.size());
        if(open.size() != 0) gå(lowestFscore());
    }

    void finnUtvei(){
        resetAll();
        start = this;
        open.add(start);
        gå(start);
    }

    private String koordinater(){
        return "(" + x + ", " + y + ")";
    }

    private String backTrack(String historikk) {
        Rute currentRute = this;
        setTegn("\u001B[32m██\u001B[0m");
        if (currentRute == start){
            return currentRute.koordinater() + historikk;
        } else {
            historikk = " --> " + currentRute.koordinater() + historikk;
        }
        return forrige.backTrack(historikk);
    }

    private Rute lowestFscore(){
        // brukes til å finne den ruten som har best potensiale
        // til å være en rute på den korteste veien til en utvei.
        Rute lowest = open.get(0);
        for(int i = 1; i < open.size(); i++){
            if(open.get(i).fScore() < lowest.fScore()){
                lowest = open.get(i);
            }
        }
        return lowest;
    }

    private double fScore(){
        return distStart + distEnd;
    }

    private void resetAll(){
        for(int x = 0; x < labyrint.rader; x++) {
            for (int y = 0; y < labyrint.kolonner; y++) {
                labyrint.ruter[x][y].reset();
            }
        }
    }

    private double avstandTil(Rute til){
        int x = til.x - this.x;
        int y = til.y - this.y;
        // Sqrt(x^2+y^2),  lengden av en vektor
        return Math.sqrt(Math.pow(x,2)+Math.pow(y,2));
    }

    private double avstandTilNærmesteUtvei(){
        Rute nærmest = labyrint.utveiRuter.hent(0);
        double avstand = avstandTil(nærmest);
        for(int i = 1; i < labyrint.utveiRuter.stoerrelse(); i++){
            double x = avstandTil(labyrint.utveiRuter.hent(i));
            if(avstand > x){
                avstand = x;
            }
        }
        return avstand;
    }
}
