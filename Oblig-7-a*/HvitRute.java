class HvitRute extends Rute{

    public HvitRute(int x, int y, Labyrint labyrint){
        super(x,y, labyrint);
        setTegn("\u001B[37m██\u001B[0m");
    }

    @Override
    char tilTegn() {return '.';}

    @Override
    public String toString() {return tegn;}

    @Override
    void reset(){
        closed.remove(this);
        setTegn("\u001B[37m██\u001B[0m");
    }
}