import java.io.*;
import java.util.Scanner;

class Labyrint {
    int kolonner;
    int rader;
    boolean detaljert = false;
    Liste<Rute> utveiRuter = new Lenkeliste<>();
    Rute[][] ruter;
    Liste<String> utveier = new Lenkeliste<>();

    private Labyrint(int kol, int rad){
        rader = kol;
        kolonner = rad;
    }

    public static Labyrint lesFraFil (File fil) throws FileNotFoundException {
        Scanner sc = new Scanner(fil);
        int kolonner = sc.nextInt();
        int rader = sc.nextInt();
        Labyrint labyrint = new Labyrint(kolonner, rader);
        labyrint.ruter = labyrint.lagRutenett(sc, labyrint);

        return labyrint;
    }

    private Rute[][] lagRutenett(Scanner sc, Labyrint labyrint){
        Rute[][] ruter = new Rute[rader][kolonner];
        for(int x = 0; x < rader; x++){
            String rad = sc.next();
            for(int y = 0; y < kolonner; y++){
                char rute = rad.charAt(y);
                if (rute == '#') {
                    ruter[x][y] = new SortRute(x, y, labyrint);
                }
                else if (rute == '.') {
                    if (x == 0 || x == rader -1 || y == 0 || y == kolonner -1){
                        ruter[x][y] = new Åpning(x, y, labyrint);
                        utveiRuter.leggTil(ruter[x][y]);
                    } else {
                        ruter[x][y] = new HvitRute(x, y, labyrint);
                    }
                }
            }
        }
        leggTilNaboer(ruter);
        return ruter;
    }

    private void leggTilNaboer(Rute[][] ruter){
        for(int x = 0; x < rader; x++){
            for(int y = 0; y < kolonner; y++) {
                Rute[] naboer = new Rute[4];
                if (x-1 >= 0) naboer[0] = ruter[x-1][y]; // North
                if (x+1 < ruter.length) naboer[1] = ruter[x+1][y]; // South
                if (y-1 >= 0) naboer[2] = ruter[x][y-1]; // West
                if (y+1 < ruter[0].length) naboer[3] = ruter[x][y+1]; // East
                ruter[x][y].naboer = naboer;
            }
        }
    }

    public Liste<String> finnUtveiFra(int kol, int rad){
        utveier = new Lenkeliste<>();
        ruter[rad][kol].finnUtvei();
        return utveier;
    }

    public void skrivUt(){
        for (int i = 0; i < ruter.length; i++){
            for (int j = 0; j < ruter[0].length; j++){
                System.out.print(ruter[i][j].toString());
            }
            System.out.println();
        }
    }
}