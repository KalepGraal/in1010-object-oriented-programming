import java.io.File;
import java.io.FileNotFoundException;

public class Speedtest {
    public static void main(String[] args) {
        int numTests = 100;
        /*for (int i = 1; i <= 7; i++) {
            speedtest("labyrinter/" + i + ".in", numTests);
        }
        speedtest("labyrinter/enkel1.in", numTests);
        speedtest("labyrinter/enkel2.in", numTests);
        speedtest("labyrinter/enkel3.in", numTests);
        speedtest("labyrinter/syklisk1.in", numTests);
        speedtest("labyrinter/syklisk2.in", numTests);
        speedtest("labyrinter/syklisk3.in", numTests);*/
        speedtest("labyrinter/enkel4.in", numTests);
    }

    static void speedtest(String filepath, int tests){
        try {
            Labyrint labyrint = Labyrint.lesFraFil(new File(filepath));
            double average = 0;
            for (int j = 0; j < tests; j++) {
                //Long startTime = System.currentTimeMillis();
                Long startTime = System.nanoTime();
                labyrint.finnUtveiFra(1,1);
                //Long runtime = System.currentTimeMillis() - startTime;
                Long runtime = System.nanoTime() - startTime;
                //System.out.println("Runtime: " + runtime + "ms");
                average += runtime;
            }
            average = average / tests;
            double r = average / 1000000;
            System.out.printf(filepath + ", avg: %.2fms \n", r);


        } catch (FileNotFoundException ex) {
            System.out.println("Fant ikke fila, avslutter");
            System.exit(1);
        }
    }

}