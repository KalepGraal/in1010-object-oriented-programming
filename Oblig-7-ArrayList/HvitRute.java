import javafx.scene.paint.Color;

class HvitRute extends Rute{

    public HvitRute(int x, int y, Labyrint labyrint){
        super(x,y, labyrint);
    }

    @Override
    char tilTegn() {return '.';}

    @Override
    Color farge() {return Color.WHITE;}

    @Override
    void onClick(Gui gui){
        gui.ruteSelected = true;
        gui.labyrint.finnUtveiFra(x, y);
        gui.utveiNr = 0;
        System.out.println("Fant " + labyrint.utveier.size() + " utveier");
        gui.infoText.setText("Utvei " + (gui.utveiNr+1) + "/" + labyrint.utveier.size());
        gui.tegnUtvei();
    }

    @Override
    void gå(){
        sjekket.add(this);

        for(Rute nabo : naboer){
            if(sjekket.contains(nabo)) continue;

            double nyAvstandTilNabo = distStart + avstandTil(nabo);
            if (nyAvstandTilNabo < nabo.distStart || !skalSjekkes.contains(nabo)){
                nabo.forrige = this;
                nabo.distStart = nyAvstandTilNabo;
                nabo.distEnd = nabo.avstandTilNærmesteUtvei();
                if (!skalSjekkes.contains(nabo)){
                    skalSjekkes.add(nabo);
                }
            }
        }
        if(skalSjekkes.size() != 0) removeLowestFScore().gå();
    }
}