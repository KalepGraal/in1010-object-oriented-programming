Deltar i konkurranse for beste/raskeste løsningsalgoritme.
Main er i Gui, men regner med dere ikke bruker den...

Stack må økes dersom programmet kjøres på større labyrinter.
(Testet med -Xss16M)

Har benyttet meg av en variant av A* algoritmen som utgangspunkt.
Algoritmen funker slik:
    Den starter på en hvit rute, også sjekker den hvilken av nabo-rutene 
    (sjekker kun hvite ruter), som virker mest sannsynlig å være på den korteste veien
    til en utvei. Dette gjøres ved å ta avstanden til start addert med avstanden til 
    den nærmeste utveien. Dette utgjør rutens "fScore". Når alle nabo-rutene får sin fScore
    oppdatert, går gå() videre til ruten med lavest fScore.
    Dersom to ruter har lik fScore, er det avstand til nærmeste utvei som avgjør hvilken det gås til.
    Alle ruter har en referanse til sin forrige rute, denne referansen endres dersom det 
    oppdages en nabo rute som har en kortere vei til start-ruten. Når den omsider kommer til en
    utvei, går den igjennom alle rutene sine forrige-referanser til den kommer til null (start-ruten).
    Så legges denne utveien til i listen med utveier. Dersom antallet elementer i listen med utveier er 
    lik antallet utveier, så avsluttes programmet.

