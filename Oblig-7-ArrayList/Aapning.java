class Aapning extends HvitRute {
    Aapning(int x, int y, Labyrint labyrint){
        super(x,y, labyrint);
        labyrint.antÅpninger++;
    }

    @Override
    void gå(){
        sjekket.add(this);
        labyrint.utveier.add(backTrack(""));
        if(labyrint.antÅpninger > labyrint.utveier.size()){
            if(skalSjekkes.size() != 0) removeLowestFScore().gå();
        }
    }
}