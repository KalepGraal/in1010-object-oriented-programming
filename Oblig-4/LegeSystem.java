public class LegeSystem {
    private static LegeSystem ls = new LegeSystem();
    private Database db = new Database();
    private Skriver skriver = new Skriver(db);
    private DataBehandler dataBehandler = new DataBehandler(db, skriver);

    public static void main(String[] args){
        ls.start();
    }

    private void start(){
        skriver.skrivHovedMeny();
        int menyValg = DataHenter.hentIntInput("Valg",4);
        velg(menyValg);
    }

    private void velg(int valg){
        if(valg == 0) skriver.skrivUtData();
        else if(valg == 1) dataBehandler.leggTil();
        else if(valg == 2) dataBehandler.brukEnResept();
        else if(valg == 3) skriver.skrivStatistikk();
        else if(valg == 4) avslutt();
        start();
    }

    private void avslutt(){
        System.out.println("\nAvslutter.");
        System.exit(1);
    }
}