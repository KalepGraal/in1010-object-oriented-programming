public class Database {
    public static Liste<Pasient> pasienter = new Lenkeliste<>();
    public static Liste<Legemiddel> legemidler = new Lenkeliste<>();
    public static Liste<Resept> resepter = new Lenkeliste<>();
    public static Liste<Lege> leger = new SortertLenkeliste<>();

    public Database(){
        // Test-program
        pasienter.leggTil(new Pasient("Ola Nordmann", "010203-43125"));
        pasienter.leggTil(new Pasient("Kari Nordmann", "010203-43126"));

        legemidler.leggTil(new LegemiddelA("Nark", 30, 5, 5));
        legemidler.leggTil(new LegemiddelB("Vanedannende", 50, 3, 7));

        leger.leggTil(new Lege("Dr. House"));
        leger.leggTil(new FastLege("Dr. Frisk", 42));

        resepter.leggTil(new BlåResept(legemidler.hent(0), leger.hent(0), pasienter.hent(0), 5));
        resepter.leggTil(new Militærresept(legemidler.hent(1), leger.hent(1), pasienter.hent(1), 3));
        resepter.leggTil(new HvitResept(legemidler.hent(1), leger.hent(0), pasienter.hent(1), 1));
    }

    public Liste<Pasient> hentPasienter(){
        return pasienter;
    }
    public Liste<Legemiddel> hentLegemidler(){
        return legemidler;
    }
    public Liste<Resept> hentResepter(){
        return resepter;
    }
    public Liste<Lege> hentLeger(){
        return leger;
    }
}