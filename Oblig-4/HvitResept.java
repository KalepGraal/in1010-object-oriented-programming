class HvitResept extends Resept {

    public HvitResept(Legemiddel legemiddel, Lege utskrivendeLege, Pasient pasient, int reit){
        super(legemiddel, utskrivendeLege, pasient, reit);
    }

    @Override
    public double prisAaBetale(){
        return hentLegemiddel().hentPris();
    }

    @Override
    public String farge() {
        return "hvit";
    }
}
