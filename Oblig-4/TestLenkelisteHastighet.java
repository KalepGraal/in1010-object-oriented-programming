public class TestLenkelisteHastighet {
    public static void main(String[] args){
        TestLenkeliste.main(args);

        Liste<Integer> liste = new Lenkeliste<>();
        int antGanger = 10000000;
        System.out.println("\n Starting speedtest... kjører leggTil " + antGanger + " ganger.");
        double time = System.currentTimeMillis();
        for (int i = 0; i < antGanger; i++) {
            liste.leggTil(1);
        }

        System.out.println("Completed task in " + (System.currentTimeMillis() - time)/1000 + " Seconds");
    }
}