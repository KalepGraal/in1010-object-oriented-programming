abstract class Resept {
    private static int id = 0;
    private int unikId;
    private Legemiddel legemiddel;
    private Lege utskrivendeLege;
    private Pasient pasient;
    private int reit;


    public Resept(Legemiddel legemiddel, Lege utskrivendeLege, Pasient pasient, int reit){
        this.legemiddel = legemiddel;
        this.utskrivendeLege = utskrivendeLege;
        this.pasient = pasient;
        this.reit = reit;
        utskrivendeLege.nyResept(this);
        pasient.nyResept(this);
        unikId = id++;
    }


    abstract public String farge();
    abstract public double prisAaBetale();

    public boolean bruk(){
        if(reit > 0) {
            reit--;
            return true;
        }
        return false;
    }

    public int hentId() {
        return unikId;
    }

    public Legemiddel hentLegemiddel() {
        return legemiddel;
    }

    public Lege hentUtskrivendeLege() {
        return utskrivendeLege;
    }

    public Pasient hentPasient() {
        return pasient;
    }

    public int hentReit() {
        return reit;
    }
}
