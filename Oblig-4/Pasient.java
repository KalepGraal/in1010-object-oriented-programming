public class Pasient {
    private static int staticId;
    private Stabel<Resept> resepter = new Stabel<>();
    private String navn;
    private String fødselsNr;
    private int unikId;

    public Pasient(String navn, String fødselsNr){
        this.navn = navn;
        this.fødselsNr = fødselsNr;

        unikId = staticId;
        staticId++;
    }

    public String hentNavn(){
        return navn;
    }

    public String hentFødselsNr(){
        return fødselsNr;
    }

    public int hentId(){
        return unikId;
    }

    public Stabel<Resept> hentResepter(){
        return resepter;
    }

    public void nyResept(Resept resept){
        resepter.leggPaa(resept);
    }
}