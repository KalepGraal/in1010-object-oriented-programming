import java.util.Iterator;

public class Lenkeliste<T> implements Liste<T>{
    Node start = null;
    Node end = null;
    private int antElementer = 0;

    public int stoerrelse(){
        return antElementer;
    }

    public void leggTil(int pos, T x){
        if(pos < 0 || pos > antElementer) throw new UgyldigListeIndeks(pos);
        else if(antElementer == 0) {
            start = new Node(x);
            end = start;
        }
        else if(pos == 0) {
            Node temp = start;
            start = new Node(x);
            start.neste = temp;
        } else if(pos == antElementer){
            end.neste = new Node(x);
            end = end.neste;
        } else {
            Node n = hentNodePaaIndeks(pos - 1);
            if(n.neste == null) {
                n.neste = new Node(x);
                end = n.neste;
            }
            else {
                Node temp = n.neste;
                n.neste = new Node(x);
                n.neste.neste = temp;
            }
        }
        antElementer++;
    }

    public void leggTil(T x){
        if(antElementer == 0) {
            start = new Node(x);
            end = start;
        }
        else {
            end.neste = new Node(x);
            end = end.neste;
        }
        antElementer++;
    }

    public void sett(int pos, T x){
        Node n = hentNodePaaIndeks(pos);
        if(n == null || pos < 0){
            throw new UgyldigListeIndeks(pos);
        }
        n.data = x;
    }

    public T hent(int pos){
        if(pos < 0) throw new UgyldigListeIndeks(-1);
        Node n = hentNodePaaIndeks(pos);
        if(n == null) throw new UgyldigListeIndeks(pos);
        return n.data;
    }

    public T fjern(int pos){
        if(pos == 0) return fjern();
        else if(hentNodePaaIndeks(pos) == null || pos < 0){
            throw new UgyldigListeIndeks(pos);
        }
        Node forrige = hentNodePaaIndeks(pos - 1);
        Node fjernes = forrige.neste;
        if(fjernes == end) end = forrige;
        forrige.neste = fjernes.neste;
        antElementer--;
        return fjernes.data;
    }

    public T fjern(){
        if (start == null){
            throw new UgyldigListeIndeks(-1);
        }
        Node fjernes = start;
        start = fjernes.neste;
        antElementer--;
        return fjernes.data;
    }

    private Node hentNodePaaIndeks(int indeks){
        Node n = start;
        for(int i = 0; i < indeks; i++){
            n = n.neste;
        }
        return n;
    }

    public Iterator iterator(){return new LenkelisteIterator();}

    class Node {
        Node neste = null;
        T data;

        Node(T t){data = t;}
    }

    class LenkelisteIterator implements Iterator<T>{
        private Node iNode = start;

        @Override
        public boolean hasNext() {
            if(iNode != null) return true;
            return false;
        }
        @Override
        public T next() {
            T data = iNode.data;
            iNode = iNode.neste;
            return data;
        }

    }
}
