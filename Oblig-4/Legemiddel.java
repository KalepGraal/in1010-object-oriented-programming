class Legemiddel {
    private static int id = 0;
    private String navn;
    private int unikId;
    private double pris;
    private double virkestoff;

    public Legemiddel(String navn, double pris, double virkestoff){
        this.navn = navn;
        this.pris = pris;
        this.virkestoff = virkestoff;
        this.unikId = id++;
    }

    public int hentId(){
        return unikId;
    }

    public String hentNavn(){
        return navn;
    }

    public double hentPris(){
        return pris;
    }

    public double hentVirkestoff(){
        return virkestoff;
    }

    public void settNyPris(double nyPris){
        pris = nyPris;
    }
}
