public class Skriver {
    Database db;

    public Skriver(Database db){
        this.db = db;
    }

    public void skrivHovedMeny(){
        System.out.println("\n         Hovedmeny");
        System.out.println("     Muligheter:");
        System.out.println("[0]: Skrive ut informasjon");
        System.out.println("[1]: Legge til noe nytt");
        System.out.println("[2]: Bruke en pasients resept");
        System.out.println("[3]: Skrive ut statistikk");
        System.out.println("[4]: Avslutte");
    }

    public void skrivUtData(){
        System.out.println("Skriver ut data");
        System.out.println("\n      Pasienter ");
        System.out.println("id, navn, fødselsnr, antall resepter");
        for (Pasient p : db.hentPasienter()){
            System.out.println(p.hentId() + ", " + p.hentNavn() + ", " + p.hentFødselsNr() + ", " + p.hentResepter().stoerrelse());
        }
        System.out.println("\n      Leger: ");
        System.out.println("navn, antall resepter, (FastLege avtalenummer)");
        for (Lege l : db.hentLeger()){
            System.out.print(l.hentNavn() + ", " + l.hentResepter().stoerrelse());
            if(l instanceof FastLege) System.out.print(", " + ((FastLege) l).hentAvtalenummer());
            System.out.println();
        }
        System.out.println("\n      Legemidler");
        System.out.println("Legemiddel, id, pris, virkestoff, (styrke)");
        for (Legemiddel l : db.hentLegemidler()){
            System.out.print(l.hentNavn() + ", " + l.hentId() + ", " + l.hentPris() + ", " + l.hentVirkestoff());
            if (l instanceof LegemiddelA) System.out.print(", " + ((LegemiddelA) l).hentNarkotiskStyrke());
            else if (l instanceof LegemiddelB) System.out.print(", " + ((LegemiddelB) l).hentVanedannendeStyrke());
            System.out.println();
        }
        System.out.println("\n      Resepter");
        System.out.println("id, legemiddel, pasient, reit, utskrivende lege, type resept");
        for (Resept r : db.hentResepter()){
            System.out.print(r.hentId() + ", " + r.hentLegemiddel().hentNavn() + ", " + r.hentPasient().hentNavn() + ", " +
                                    r.hentReit() + ", " + r.hentUtskrivendeLege().hentNavn());
            if (r instanceof BlåResept) System.out.print(", Blå resept");
            else if (r instanceof PResept) System.out.print(", P-resept");
            else if (r instanceof Militærresept) System.out.print(", Militærresept");
            else if (r instanceof HvitResept) System.out.print(", Hvit resept");
            System.out.println();

        }
    }

    public void skrivStatistikk(){
        skrivStatsMeny();
        int valg = DataHenter.hentIntInput("Valg", 2);
        if (valg == 0) skrivLegeNarkStats();
        else if (valg == 1) skrivNarkPasStats();
        else if (valg == 2) System.out.println("Avslutter");
    }

    public void skrivLegeNarkStats(){
        for(Lege l : db.hentLeger()){
            int antNark = 0;
            for (Resept r : l.hentResepter()){
                if (r.hentLegemiddel() instanceof LegemiddelA){
                    antNark++;
                }
            }
            if (antNark > 0){
                System.out.println(l.hentNavn() + " har skrevet " + antNark + " narkotiske resepter.");
            }
        }
    }

    public void skrivNarkPasStats(){
        for(Pasient p : db.hentPasienter()){
            int antNark = 0;
            for (Resept r : p.hentResepter()){
                if (r.hentLegemiddel() instanceof LegemiddelA && r.hentReit() > 0){
                    antNark++;
                }
            }
            if (antNark > 0){
                System.out.println("Pasienten " + p.hentNavn() + " har " + antNark + " gyldige narkotiske resepter.");
            }
        }
    }

    public void skrivStatsMeny(){
        int antVaneResept = 0;
        int antVaneMilRes = 0;
        for (Resept r : db.hentResepter()){
            if (r.hentLegemiddel() instanceof LegemiddelB){
                antVaneResept++;
                if (r instanceof Militærresept){
                    antVaneMilRes++;
                }
            }
        }

        System.out.println("\nStats");
        System.out.println("Antall vanedannende legemiddel resepter: " + antVaneResept);
        System.out.println("Antall vanedannende resepter skrevet til militære: " + antVaneMilRes);
        System.out.println("[0] Leger som har skrevet ut resept på narkotiske legemidler");
        System.out.println("[1] Pasienter med resept på narkotiske legemidler");
        System.out.println("[2] Avbryt");
    }

    public int skrivPasienterMedResept(){
        int i = 0;
        for(Pasient p : db.hentPasienter()){
            if (p.hentResepter().stoerrelse() > 0){
                System.out.print("[" + i +  "] ");
                System.out.println("Navn: " + p.hentNavn() + ", antall resepter: " + p.hentResepter().stoerrelse());
                i++;
            }
        }
        System.out.println("[" + i + "] Avbryt");
        return i;
    }

    public void skrivPasienter(){
        int i = 0;
        for(Pasient p : db.hentPasienter()){
            System.out.print("[" + i +  "] ");
            System.out.println("Navn: " + p.hentNavn() + ", antall resepter: " + p.hentResepter().stoerrelse());
            i++;
        }
    }

    public void skrivLeger(){
        int i = 0;
        for(Lege l : db.hentLeger()){
            System.out.print("[" + i +  "] ");
            System.out.println("Navn: " + l.hentNavn());
            i++;
        }
    }

    public void skrivLegemidler(){
        int i = 0;
        for(Legemiddel l : db.hentLegemidler()){
            System.out.println("[" + i +  "]: " + l.hentNavn());
            i++;
        }
    }

    public void skrivResepter(Pasient pasient){
        int i = 0;
        for(Resept r : pasient.hentResepter()){
            System.out.print("[" + i +  "] ");
            System.out.print("Id: " + r.hentId() + ", Legemiddel: " + r.hentLegemiddel().hentNavn());
            System.out.println(", Reit: " + r.hentReit() + ", Pris: " + r.prisAaBetale() + "kr");
            i++;
        }
    }
}