class Lege implements Comparable<Lege>{
    private String navn;
    private Liste<Resept> resepter = new Lenkeliste<>();

    public Lege(String navn){
        this.navn = navn;
    }

    public int compareTo(Lege lege){
        return navn.compareToIgnoreCase(lege.hentNavn());
    }

    public void nyResept(Resept resept){
        resepter.leggTil(resept);
    }

    public Liste<Resept> hentResepter(){
        return resepter;
    }

    public String hentNavn() {
        return navn;
    }
}
