public class DataBehandler {
    private Database db;
    private Skriver skriver;

    public DataBehandler(Database db, Skriver skriver){
        this.db = db;
        this.skriver = skriver;
    }

    public void leggTil(){
        printAlternativer();
        int valg = DataHenter.hentIntInput("Valg", 4);
        velg(valg);
    }

    public void velg(int valg) {
        if(valg == 0) leggTilPasient();
        else if(valg == 1) leggTilEnLege();
        else if(valg == 2) leggTilEtLegemiddel();
        else if(valg == 3) leggTilEnResept();
        // 4 vil implisitt returnere til hovedmeny
    }

    public void printAlternativer(){
        System.out.println("Hva vil du legge til?");
        System.out.println("[0]: Pasient");
        System.out.println("[1]: Lege");
        System.out.println("[2]: Legemiddel");
        System.out.println("[3]: Resept");
        System.out.println("[4]: Avbryt");
    }

    private void leggTilPasient(){
        System.out.println("Legger til en ny pasient. 0 for å avbryte.");
        String navn = DataHenter.hentStringInput("Navn: ");
        if (navn.equals("0")) {System.out.println("Avbryter"); return;}
        String fodselsNr = DataHenter.hentStringInput("FødselsNr: ");
        if (fodselsNr.equals("0")) {System.out.println("Avbryter"); return;}
        db.hentPasienter().leggTil(new Pasient(navn, fodselsNr));
    }

    private void leggTilEnLege(){
        System.out.println("Hva vil du legge til?");
        System.out.println("[0]: Ny lege.");
        System.out.println("[1]: Ny fastlege");
        System.out.println("[2]: Avbryte");
        int valg = DataHenter.hentIntInput("Valg",2);
        if (valg == 0) leggTilLege();
        else if (valg == 1) leggTilFastlege();
        else if (valg == 2) System.out.println("Avbryter");
    }

    private void leggTilLege(){
        System.out.println("Legger til en lege. 0 for å avbryte.");
        String navn = DataHenter.hentStringInput("Navn: ");
        if (navn.equals("0")) {System.out.println("Avbryter"); return;}
        db.hentLeger().leggTil(new Lege(navn));
    }

    private void leggTilFastlege(){
        System.out.println("Legger til en fastlege. 0 for å avbryte. ");
        String navn = DataHenter.hentStringInput("Navn: ");
        if (navn.equals("0")) {System.out.println("Avbryter"); return;}
        int avtaleNr = DataHenter.hentIntInput("AvtaleNr");
        if (avtaleNr == 0) {System.out.println("Avbryter"); return;}
        db.hentLeger().leggTil(new FastLege(navn, avtaleNr));
    }

    private void leggTilEtLegemiddel(){
        System.out.println("Hva slags legemiddel vil du legge til?");
        System.out.println("[0]: Vanlig legemiddel");
        System.out.println("[1]: Vanedannende legemiddel");
        System.out.println("[2]: Narkotisk legemiddel");
        System.out.println("[3]: Avbryt");
        int svar = DataHenter.hentIntInput("Valg", 3);
        if (svar == 0) leggTilLegemiddel();
        else if (svar == 1) leggTilVanedannende();
        else if (svar == 2) leggTilNarkotisk();
        else if(svar == 3) System.out.print("Avbryter");
    }

    private void leggTilLegemiddel(){
        System.out.println("Legger til legemiddel. -1 for å avbryte.");

        String navn = DataHenter.hentStringInput("Navn: ");
        if (navn.equals("-1")) {System.out.println("Avbryter"); return;}

        double pris = DataHenter.hentDoubleInput("Pris");
        if (pris == -1) {System.out.println("Avbryter"); return;}

        double virkestoff = DataHenter.hentDoubleInput("Virkestoff");
        if (virkestoff == -1) {System.out.println("Avbryter"); return;}

        db.hentLegemidler().leggTil(new LegemiddelC(navn, pris, virkestoff));

    }

    private void leggTilNarkotisk(){
        System.out.println("Legger til et Narkotisk legemiddel. -1 for å avbryte.");

        String navn = DataHenter.hentStringInput("Navn: ");
        if (navn.equals("-1")) {System.out.println("Avbryter"); return;}

        double pris = DataHenter.hentDoubleInput("Pris");
        if (pris == -1) {System.out.println("Avbryter"); return;}

        double virkestoff = DataHenter.hentDoubleInput("Virkestoff");
        if (virkestoff == -1) {System.out.println("Avbryter"); return;}

        int styrke = DataHenter.hentIntInput("Narkotisk styrke");
        if (styrke == -1) {System.out.println("Avbryter"); return;}

        db.hentLegemidler().leggTil(new LegemiddelA(navn, pris, virkestoff, styrke));}

    private void leggTilVanedannende(){
        System.out.println("Legger til et vanedannende legemiddel. -1 for å avbryte.");

        String navn = DataHenter.hentStringInput("Navn: ");
        if (navn.equals("-1")) {System.out.println("Avbryter"); return;}

        double pris = DataHenter.hentDoubleInput("Pris");
        if (pris == -1) {System.out.println("Avbryter"); return;}

        double virkestoff = DataHenter.hentDoubleInput("Virkestoff");
        if (virkestoff == -1) {System.out.println("Avbryter"); return;}

        int styrke = DataHenter.hentIntInput("Vanedannende styrke");
        if (styrke == -1) {System.out.println("Avbryter"); return;}

        db.hentLegemidler().leggTil(new LegemiddelB(navn, pris, virkestoff, styrke));

    }

    private void leggTilEnResept(){
        System.out.println("Hva slags resept vil du legge til?");
        System.out.println("[0]: Hvit resept");
        System.out.println("[1]: Blå resept");
        System.out.println("[2]: Militærresept");
        System.out.println("[3]: P-Resept");
        System.out.println("[4]: Avbryte");

        int valg = DataHenter.hentIntInput("Valg", 4);

        if (valg == 0) leggTilResept("Hvit resept");
        else if (valg == 1) leggTilResept("Blå resept");
        else if (valg == 2) leggTilResept("Militærresept");
        else if (valg == 3) leggTilResept("P-Resept");
    }

    private void leggTilResept(String type){
        System.out.println("Legger til resept. -1 for å avbryte.");

        System.out.println("Hvilket legemiddel vil du lage en resept for?");
        skriver.skrivLegemidler();
        int valg = DataHenter.hentIntInput("Valg", db.hentLegemidler().stoerrelse() - 1);
        if (valg == -1) {System.out.println("Avbryter"); return;}
        Legemiddel legemiddel = db.hentLegemidler().hent(valg);

        System.out.println("Hvilken lege skriver ut resepten?");
        skriver.skrivLeger();
        valg = DataHenter.hentIntInput("Valg", db.hentLeger().stoerrelse() - 1);
        if (valg == -1) {System.out.println("Avbryter"); return;}
        Lege utskrivendeLege = db.hentLeger().hent(valg);

        System.out.println("Hvilken pasient skrives resepten til?");
        skriver.skrivPasienter();
        valg = DataHenter.hentIntInput("Valg", db.hentPasienter().stoerrelse() - 1);
        if (valg == -1) {System.out.println("Avbryter"); return;}
        Pasient pasient = db.hentPasienter().hent(valg);

        if (type.equals("P-Resept")) {
            db.hentResepter().leggTil(new PResept(legemiddel, utskrivendeLege, pasient));
            return;
        }

        // System.out.println("Hvor mange reit har resepten?");
        int reit = DataHenter.hentIntInput("Reit");
        if (reit == -1) {System.out.println("Avbryter"); return;}

        if (type.equals("Hvit resept")) db.hentResepter().leggTil(new HvitResept(legemiddel, utskrivendeLege, pasient, reit));
        else if (type.equals("Blå resept")) db.hentResepter().leggTil(new BlåResept(legemiddel, utskrivendeLege, pasient, reit));
        else if (type.equals("Militærresept")) db.hentResepter().leggTil(new Militærresept(legemiddel, utskrivendeLege, pasient, reit));
    }

    public void brukEnResept(){
        System.out.println("Hvilken pasient vil du bruke en resept for?");
        int pasMedRes = skriver.skrivPasienterMedResept();

        // System.out.println("["+ db.hentPasienter().stoerrelse() + "] Avbryt");
        int valg = DataHenter.hentIntInput("Valg", db.hentPasienter().stoerrelse());
        if (valg == pasMedRes) {
            System.out.println("Avbryter");
            return;
        }
        Pasient pasient = db.hentPasienter().hent(valg);
        brukEnPasientsResept(pasient);
    }

    public void brukEnPasientsResept(Pasient pasient){
        System.out.println("Hvilken resept vil du bruke?");
        skriver.skrivResepter(pasient);

        System.out.println("["+ pasient.hentResepter().stoerrelse() + "] Avbryt");
        int valg = DataHenter.hentIntInput("Valg", pasient.hentResepter().stoerrelse());
        if (valg == pasient.hentResepter().stoerrelse()){
            System.out.println("Avbryter");
            return;
        }

        Resept resept = pasient.hentResepter().hent(valg);
        if(resept.hentReit() > 0){
            resept.bruk();
            System.out.println("Brukte resepten. Pris å betale: " + resept.prisAaBetale() +  "kr. Gjenværende reit: " + resept.hentReit());
        } else {
            System.out.println("Kan ikke bruke en resept med 0 gjenværende reit\n");
            brukEnPasientsResept(pasient);
        }
    }
}