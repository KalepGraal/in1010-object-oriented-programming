class Militærresept extends HvitResept {
    private double rabatt = 1.0; // beholde denne og hentRabatt, eller prisEtterRabatt?

    public Militærresept(Legemiddel legemiddel, Lege utskrivendeLege, Pasient pasient, int reit){
        super(legemiddel, utskrivendeLege, pasient, reit);
    }

    @Override
    public double prisAaBetale() {
        return hentLegemiddel().hentPris() * (1.0 - rabatt);
    }
}
