public class minLenkelisteIteratorTest {
    public static void main(String[] args){
        Liste<Lege> liste = new SortertLenkeliste<>();
        liste.leggTil(new Lege("Lege B"));
        liste.leggTil(new Lege("Lege C"));
        liste.leggTil(new Lege("Lege A"));

        for (Lege s : liste){
            System.out.println(s.hentNavn());
        }
    }
}