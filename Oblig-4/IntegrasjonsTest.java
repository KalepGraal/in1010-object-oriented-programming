public class IntegrasjonsTest {

    /*
    * Opprette minimum en instans av hver eneste klasse og la disse inneholde
    * nødvendige referanser til andre objekter.
    * Skrive ut all tilgjengelig informasjon om hvert enkelt objekt
    * */

    public static void main(String args[]){
        // Instances of all classes:
        Lege lege = new Lege("Bob Kåre");
        FastLege fastLege = new FastLege("Eråk Bob", 1337);
        Pasient pasient = new Pasient("Freddy Kruger", "1122335431");

        LegemiddelA narkotisk = new LegemiddelA("Morfin", 57.31, 15.00, 10);
        LegemiddelB vanedannende = new LegemiddelB("Xanax", 252.28, 0.25, 3);
        LegemiddelC legemiddelC = new LegemiddelC("Paracetamol", 22.80, 500.00);

        HvitResept hvitResept = new HvitResept(narkotisk, lege, pasient, 2);
        BlåResept blåResept = new BlåResept(vanedannende, fastLege, pasient, 2);
        PResept pResept = new PResept(legemiddelC, lege, pasient);
        Militærresept militærresept = new Militærresept(narkotisk, fastLege, pasient, 4);

        System.out.println("Lege navn: " + lege.hentNavn());
        System.out.println("FastLege navn: " + fastLege.hentNavn());
        System.out.println("FastLege avtaleNr: " + fastLege.hentAvtalenummer());

        System.out.println("\nHvitResept");
        printResept(hvitResept);

        System.out.println("\nBlåResept");
        printResept(blåResept);

        System.out.println("\nPResept");
        printResept(pResept);

        System.out.println("\nMilitærresept");
        printResept(militærresept);
    }

    public static void printResept(Resept resept){
        System.out.println("- farge: " + resept.farge());
        System.out.println("- resept id: " + resept.hentId());
        System.out.println("- pasientNavn: " + resept.hentPasient().hentNavn());
        System.out.println("- legemiddel: " + resept.hentLegemiddel().hentNavn());
        System.out.println("- utskrivende lege: " + resept.hentUtskrivendeLege().hentNavn());
        System.out.println("- reit: " + resept.hentReit());
        System.out.println("- pris å betale: " + resept.prisAaBetale());
    }
}