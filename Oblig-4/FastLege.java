class FastLege extends Lege implements Kommuneavtale{
    private int avtalenummer;

    public FastLege(String navn, int avtalenummer){
        super(navn);
        this.avtalenummer = avtalenummer;
    }

    @Override
    public int hentAvtalenummer(){
        return avtalenummer;
    }
}
