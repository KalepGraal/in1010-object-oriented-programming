class PResept extends HvitResept {
    private static int fastReit = 3;  // static for å kunne brukes i konstruktøren
    private double rabatt = 116.0;

    public PResept(Legemiddel legemiddel, Lege utskrivendeLege, Pasient pasient){
        super(legemiddel, utskrivendeLege, pasient, fastReit);
    }

    @Override
    public double prisAaBetale() {
        double pris = hentLegemiddel().hentPris();
        pris -= rabatt;
        if (pris < 0.0) return 0.0;
        return pris;
    }
}
