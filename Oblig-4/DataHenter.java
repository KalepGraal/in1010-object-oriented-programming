import java.util.Scanner;

public class DataHenter {

    public static int hentIntInput(String egenskap, int max){
        try {
            Scanner sc = new Scanner(System.in);
            System.out.print("\n" + egenskap + ": ");

            int valg = sc.nextInt();
            System.out.println();
            if (valg > max || valg < -1) {
                throw new Exception();
            }
            return valg;
        } catch (Exception e){
            System.out.println();
            System.out.println("Ugyldig valg, prøv igjen.");
            return hentIntInput(egenskap, max);
        }
    }

    public static int hentIntInput(String egenskap){
        try {
            Scanner sc = new Scanner(System.in);
            System.out.print("\n" + egenskap + ": ");

            int valg = sc.nextInt();
            System.out.println();
            return valg;
        } catch (Exception e){
            System.out.println();
            System.out.println("Ugyldig valg, prøv igjen.");
            return hentIntInput(egenskap);
        }
    }

    public static double hentDoubleInput(String egenskap){
        try {
            Scanner sc = new Scanner(System.in);
            System.out.print("\n" + egenskap + ": ");
            double valg = sc.nextDouble();
            System.out.println();
            if (valg < 0 && valg != -1) {
                throw new Exception();
            }
            return valg;
        } catch (Exception e){
            System.out.println();
            System.out.println("Ugyldig valg, prøv igjen.");
            return hentDoubleInput(egenskap);
        }
    }


    public static String hentStringInput(String egenskap){
        Scanner sc = new Scanner(System.in);
        System.out.print("\n" + egenskap + ": ");
        return sc.nextLine();
    }
}