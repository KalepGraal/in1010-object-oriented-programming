public class TestLegemiddel {

    // Tester alle legemidlene, og sjekker om alle verdiene de har stemmer med det som er forventet.

    private static int testCounter = 0;

    public static void main(String[] args) {

        LegemiddelA narkotisk = new LegemiddelA("Morfin", 57.31, 15.00, 10);
        LegemiddelB vanedannende = new LegemiddelB("Xanax", 252.28, 0.25, 3);
        LegemiddelC legemiddelC = new LegemiddelC("Paracetamol", 22.80, 500.00);


        // Tester for A
        System.out.println("Tester for A");
        testEtLegemiddel(narkotisk, 0, "Morfin", 57.31, 15.00);
        testVerdi(narkotisk.hentNarkotiskStyrke(), 10);

        // Tester for B
        System.out.println("\nTester for B");
        testEtLegemiddel(vanedannende, 1, "Xanax", 252.28, 0.25);
        testVerdi(vanedannende.hentVanedannendeStyrke(), 3);

        // Tester for C
        System.out.println("\nTester for C");
        testEtLegemiddel(legemiddelC, 2, "Paracetamol", 22.80, 500.00);

    }
    public static void testVerdi(double faktiskVerdi, double forventetVerdi){
        if(faktiskVerdi == forventetVerdi){
            System.out.println("Test " + testCounter + ": Korrekt");
        } else {
            System.out.println("Test " + testCounter + ": Feil");
        }
        testCounter++;
    }
    public static void testVerdi(String faktiskVerdi, String forventetVerdi){
        if(faktiskVerdi.equals(forventetVerdi)){
            System.out.println("Test " + testCounter + ": Korrekt");
        } else {
            System.out.println("Test " + testCounter + ": Feil");
        }
        testCounter++;
    }

    public static void testEtLegemiddel(Legemiddel legemiddel, int forventetId, String forventetNavn, double forventetPris, double forventetVirkestoff){
        testCounter = 0;
        testVerdi(legemiddel.hentId(), forventetId);
        testVerdi(legemiddel.hentNavn(), forventetNavn);
        testVerdi(legemiddel.hentPris(), forventetPris);
        testVerdi(legemiddel.hentVirkestoff(), forventetVirkestoff);
        legemiddel.settNyPris(49.00);
        testVerdi(legemiddel.hentPris(), 49.00);
    }

}
