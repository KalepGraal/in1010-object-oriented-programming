class Militærresept extends HvitResept {
    private double rabatt = 1.0; // beholde denne og hentRabatt, eller prisEtterRabatt?

    public Militærresept(Legemiddel legemiddel, Lege utskrivendeLege, int pasientId, int reit){
        super(legemiddel, utskrivendeLege, pasientId, reit);
    }

    @Override
    public double prisAaBetale() {
        return hentLegemiddel().hentPris() * (1.0 - rabatt);
    }
}
