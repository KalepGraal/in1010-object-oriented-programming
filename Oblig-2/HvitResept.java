class HvitResept extends Resept {

    public HvitResept(Legemiddel legemiddel, Lege utskrivendeLege, int pasientId, int reit){
        super(legemiddel, utskrivendeLege, pasientId, reit);
    }

    @Override
    public double prisAaBetale(){
        return hentLegemiddel().hentPris();
    }

    @Override
    public String farge() {
        return "hvit";
    }
}
