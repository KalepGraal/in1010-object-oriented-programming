class BlåResept extends Resept {
    private double rabatt = 0.75;

    public BlåResept(Legemiddel legemiddel, Lege utskrivendeLege, int pasientId, int reit){
        super(legemiddel, utskrivendeLege, pasientId, reit);
    }

    @Override
    public String farge() {
        return "blaa";
    }

    @Override
    public double prisAaBetale() {
        double pris = hentLegemiddel().hentPris();
        pris = pris * (1-rabatt);
        return pris;
    }
}
