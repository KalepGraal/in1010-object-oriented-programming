class PResept extends HvitResept {
    private double rabatt = 116.0;
    private static int fastReit = 3;  // static for å kunne brukes i konstruktøren

    public PResept(Legemiddel legemiddel, Lege utskrivendeLege, int pasientId){
        super(legemiddel, utskrivendeLege, pasientId, fastReit);
    }

    @Override
    public double prisAaBetale() {
        double pris = hentLegemiddel().hentPris();
        pris -= rabatt;
        if (pris < 0.0) return 0.0;
        return pris;
    }
}
