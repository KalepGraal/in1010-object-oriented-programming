public class TestResepter {

    // Tester alle reseptene, og sjekker om alle verdiene de har stemmer med det som er forventet.

    private static int testCounter = 0;

    public static void main(String[] args) {
        Legemiddel legemiddel = new LegemiddelA("Ibuprofen", 100, 40, 2);
        Lege lege = new Lege("Ola Nordmann");

            // Tester for Resept og HvitResept
        System.out.println("Tester for HvitResept (og resept generelt)");
        HvitResept hvitResept = new HvitResept(legemiddel, lege, 1, 1);
        testVerdi(hvitResept.hentId(), 0);
        testVerdi(hvitResept.hentLegemiddel().hentNavn(), legemiddel.hentNavn());
        testVerdi(hvitResept.hentUtskrivendeLege().hentNavn(), lege.hentNavn());
        testVerdi(hvitResept.hentPasientId(), 1);
        testVerdi(hvitResept.hentReit(), 1);
        testVerdi(hvitResept.bruk(), true);
        testVerdi(hvitResept.hentReit(), 0);
        testVerdi(hvitResept.bruk(), false);
        testVerdi(hvitResept.farge(), "hvit");
        testVerdi(hvitResept.prisAaBetale(), 100);

            // Tester for BlåResept
        System.out.println("\nTester for BlåResept");
        testCounter = 0;
        BlåResept blåResept = new BlåResept(legemiddel,lege,1,1);
        testVerdi(blåResept.farge(), "blaa");
        testVerdi(blåResept.prisAaBetale(), 25.00);

            // Tester for Militærresept
        System.out.println("\nTester for Militærresept");
        testCounter = 0;
        Militærresept militærresept = new Militærresept(legemiddel, lege, 1, 2);
        testVerdi(militærresept.prisAaBetale(), 0);
        testVerdi(militærresept.farge(), "hvit");


            // Tester for PResept
        System.out.println("\nTester for PResept");
        testCounter = 0;
        PResept pResept = new PResept(legemiddel, lege, 1);
        testVerdi(pResept.prisAaBetale(), 0.0);
        legemiddel.settNyPris(200);
        testVerdi(pResept.prisAaBetale(), 84.0);
        testVerdi(pResept.farge(), "hvit");
        testVerdi(pResept.bruk(), true);
        testVerdi(pResept.bruk(), true);
        testVerdi(pResept.bruk(), true);
        testVerdi(pResept.bruk(), false);

    }

    public static void testVerdi(double faktiskVerdi, double forventetVerdi){
        if(faktiskVerdi == forventetVerdi){
            System.out.println("Test " + testCounter + ": Korrekt");
        } else {
            System.out.println("Test " + testCounter + ": Feil");
        }
        testCounter++;
    }
    public static void testVerdi(String faktiskVerdi, String forventetVerdi){
        if(faktiskVerdi.equals(forventetVerdi)){
            System.out.println("Test " + testCounter + ": Korrekt");
        } else {
            System.out.println("Test " + testCounter + ": Feil");
        }
        testCounter++;
    }
    public static void testVerdi(boolean faktiskVerdi, boolean forventetVerdi){
        if(faktiskVerdi == forventetVerdi){
            System.out.println("Test " + testCounter + ": Korrekt");
        } else {
            System.out.println("Test " + testCounter + ": Feil");
        }
        testCounter++;
    }
}
