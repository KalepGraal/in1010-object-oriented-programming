abstract class Resept {
    private static int id = 0;
    private int unikId;
    private Legemiddel legemiddel;
    private Lege utskrivendeLege;
    private int pasientId;
    private int reit;


    public Resept(Legemiddel legemiddel, Lege utskrivendeLege, int pasientId, int reit){
        this.legemiddel = legemiddel;
        this.utskrivendeLege = utskrivendeLege;
        this.pasientId = pasientId;
        this.reit = reit;
        unikId = id++;
    }


    abstract public String farge();
    abstract public double prisAaBetale();

    public boolean bruk(){
        if(reit > 0) {
            reit--;
            return true;
        }
        return false;
    }

    public int hentId() {
        return unikId;
    }

    public Legemiddel hentLegemiddel() {
        return legemiddel;
    }

    public Lege hentUtskrivendeLege() {
        return utskrivendeLege;
    }

    public int hentPasientId() {
        return pasientId;
    }

    public int hentReit() {
        return reit;
    }
}
