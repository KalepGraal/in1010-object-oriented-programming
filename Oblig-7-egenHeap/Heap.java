public class Heap<T extends Rute> {
    private T[] liste;
    //private ArrayList<T> liste = new ArrayList<>();
    private int antNoder;

    @SuppressWarnings("unchecked")
    Heap(int maksSize){

        liste = (T[]) new Rute[maksSize];
        //liste = (T[];
    }

    public void add(T t){
        t.heapIndex = antNoder;
        liste[antNoder] = t;
        sortUp(t);
        antNoder++;
    }

    T removeFirst(){
        T first = liste[0];
        antNoder--;
        liste[0] = liste[antNoder];
        liste[0].heapIndex = 0;
        sortDown(liste[0]);
        return first;
    }

    void updateItem(T item){
        sortUp(item);
    }

    public int size(){
        return antNoder;
    }

    boolean contains(T item){
        return item.equals(liste[item.heapIndex]);
    }

    private void sortDown(T item) {
        while (true) {
            int childIndexLeft = item.heapIndex * 2 + 1;
            int childIndexRight = item.heapIndex * 2 + 2;
            int swapIndex;

            if (childIndexLeft < antNoder) {
                swapIndex = childIndexLeft;

                if (childIndexRight < antNoder) {
                    if (liste[childIndexLeft].compareTo(liste[childIndexRight]) > 0) { //
                        swapIndex = childIndexRight;
                    }
                }

                if (item.compareTo(liste[swapIndex]) > 0) { //
                    swap(item, liste[swapIndex]);
                }
                else {
                    return;
                }

            }
            else {
                return;
            }

        }
    }

    private void sortUp(T item) {
        int parentIndex = (item.heapIndex-1)/2;

        while (true) {
            T parentItem = liste[parentIndex];
            if (item.compareTo(parentItem) < 0) { //
                swap(item,parentItem);
            }
            else {
                break;
            }

            parentIndex = (item.heapIndex-1)/2;
        }
    }

    private void swap(T itemA, T itemB) {
        liste[itemA.heapIndex] =  itemB;
        liste[itemB.heapIndex] =  itemA;
        int itemAIndex = itemA.heapIndex;
        itemA.heapIndex = itemB.heapIndex;
        itemB.heapIndex = itemAIndex;
    }
}