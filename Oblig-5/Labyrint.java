import java.io.*;
import java.util.Scanner;

class Labyrint {
    Rute[][] ruter;
    Liste<String> utveier = new Lenkeliste<>();
    private int rader, kolonner;

    private Labyrint(int kol, int rad){
        rader = kol;
        kolonner = rad;
    }

    static Labyrint lesFraFil (File fil) throws FileNotFoundException {
        Scanner sc = new Scanner(fil);
        int kolonner = sc.nextInt();
        int rader = sc.nextInt();
        Labyrint labyrint = new Labyrint(kolonner, rader);
        labyrint.ruter = labyrint.lagRutenett(sc, labyrint);

        return labyrint;
    }

    private Rute[][] lagRutenett(Scanner sc, Labyrint labyrint){
        // lager og returnerer et rutenett med rutene fra fil
        Rute[][] ruter = new Rute[rader][kolonner];
        for(int x = 0; x < rader; x++){
            String rad = sc.next();
            for(int y = 0; y < kolonner; y++){
                char rute = rad.charAt(y);
                if (rute == '#') {
                    ruter[x][y] = new SortRute(x, y, labyrint);
                }
                else if (rute == '.') {
                    if (x == 0 || x == rader -1 || y == 0 || y == kolonner -1){
                        ruter[x][y] = new Åpning(x, y, labyrint);
                    } else {
                        ruter[x][y] = new HvitRute(x, y, labyrint);
                    }
                }
            }
        }
        leggTilNaboer(ruter);
        return ruter;
    }

    private void leggTilNaboer(Rute[][] ruter){
        // Går igjennom alle rutene i et rutenett, og lagrer referanser til rutens naboer i hver rute.
        for(int x = 0; x < rader; x++){
            for(int y = 0; y < kolonner; y++) {
                Lenkeliste<Rute> naboer = new Lenkeliste<>();
                if (x-1 >= 0) naboer.leggTil(ruter[x-1][y]); // North
                if (x+1 < ruter.length) naboer.leggTil(ruter[x+1][y]); // South
                if (y-1 >= 0) naboer.leggTil(ruter[x][y-1]); // West
                if (y+1 < ruter[0].length) naboer.leggTil(ruter[x][y+1]); // East
                ruter[x][y].naboer = naboer;
            }
        }
    }

    Liste<String> finnUtveiFra(int kol, int rad){
        utveier = new Lenkeliste<>();
        ruter[rad][kol].finnUtvei();
        return utveier;
    }

    public void skrivUt(){
        for (Rute[] rad : ruter) {
            for (Rute rute : rad){
                System.out.print(rute.tilTegn());
            }
            System.out.println();
        }
    }

}