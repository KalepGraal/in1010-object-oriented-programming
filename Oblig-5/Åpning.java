public class Åpning extends HvitRute {
    Åpning(int x, int y, Labyrint labyrint){
        super(x,y, labyrint);
    }

    @Override
    public void gå(String historikk){
        gådd = true;
        labyrint.utveier.leggTil(historikk + koordinater());
    }

}