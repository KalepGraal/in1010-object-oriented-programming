class HvitRute extends Rute{

    HvitRute(int x, int y, Labyrint labyrint){
        super(x,y, labyrint);
    }

    @Override
    public void gå(String historikk){
        gådd = true;
        for(Rute nabo : naboer){
            if (!nabo.gådd){
                nabo.gå(historikk + koordinater() + " --> ");
            }
        }
    }

    @Override
    char tilTegn() {return '.';}

}