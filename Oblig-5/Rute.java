abstract class Rute {
    Labyrint labyrint;
    Lenkeliste<Rute> naboer;
    boolean gådd = false;
    private int kolonne, rad;

    Rute(int rad, int kolonne, Labyrint labyrint) {
        this.rad = rad;
        this.kolonne = kolonne;
        this.labyrint = labyrint;
    }

    abstract char tilTegn();

    abstract void gå(String historikk);

    void finnUtvei(){
        clear();
        gå("");
    }

    private void clear() {
        for (int x = 0; x < labyrint.ruter.length; x++) {
            for (int y = 0; y < labyrint.ruter[0].length; y++) {
                labyrint.ruter[x][y].gådd = false;
            }
        }
    }

    String koordinater(){
        return "(" + kolonne + ", " + rad + ")";
    }
}
