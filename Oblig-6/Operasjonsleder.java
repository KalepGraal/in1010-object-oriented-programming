import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Operasjonsleder implements Runnable {

    private List<List<Melding>> meldinger = new ArrayList<>();
    private Monitor monitorUkryptert;

    Operasjonsleder(Monitor monitorUkryptert, int antKanaler){
        for (int i = 0; i < antKanaler; i++) {
            meldinger.add(new ArrayList<>());
        }
        this.monitorUkryptert = monitorUkryptert;
    }

    @Override
    public void run() {
        hentMeldinger();

        for(List<Melding> l : meldinger){
            Collections.sort(l);
        }

        skrivTilFiler();
    }

    private void hentMeldinger(){
        try {
            Melding melding = monitorUkryptert.hentMelding();
            while(melding != null){
                meldinger.get(melding.getKanalId() - 1).add(melding);
                melding = monitorUkryptert.hentMelding();
            }
        } catch (InterruptedException ex){
            ex.printStackTrace();
        }
    }

    private void skrivTilFiler(){
        for (int i = 0; i < meldinger.size(); i++) {
            try {
                PrintWriter printWriter = new PrintWriter("Tekst " + (i+1) + ".txt", "UTF-8");
                for (int j = 0; j < meldinger.get(i).size(); j++) {
                    printWriter.println(meldinger.get(i).get(j).getMelding() + "\n");
                }
                printWriter.close();
            } catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }
}