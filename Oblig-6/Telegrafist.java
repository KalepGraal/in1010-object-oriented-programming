import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Telegrafist implements Runnable {

    private static Lock lytteLås = new ReentrantLock();

    private Kanal kanal;
    private int kanalId;
    private Monitor monitorKryptert;
    private int sekvensnummer;

    Telegrafist(Kanal kanal, Monitor monitorKryptert){
        this.kanal = kanal;
        this.kanalId = kanal.hentId();
        this.monitorKryptert = monitorKryptert;
    }

    @Override
    public void run() {
        //Sender monitorKryptert meldinger, så lenge det er flere meldinger i kanalen.
        //Sender null når alle meldinger er levert.

        String s = lytt();
        while(s != null){
            monitorKryptert.nyMelding(new Melding(s, sekvensnummer++, kanalId));
            s = lytt();
        }
        monitorKryptert.nyMelding(null);
    }

    // Brukes for å gjøre kanal.lytt() thread-safe
    private String lytt(){
        lytteLås.lock();
        try {
            return kanal.lytt();
        } finally {
            lytteLås.unlock();
        }
    }
}