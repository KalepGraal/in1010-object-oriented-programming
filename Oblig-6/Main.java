public class Main {
    private static int maksKanaler = 1;
    private static Operasjonssentral ops = new Operasjonssentral(maksKanaler);
    private static Kanal[] kanaler = ops.hentKanalArray();

    public static void main(String[] args){

        int antKryptografer = 20;
        int antTelegrafister = ops.hentAntallKanaler();

        Monitor monitorUkryptert = new Monitor(antKryptografer);
        Monitor monitorKryptert = new Monitor(antTelegrafister);

        Operasjonsleder operasjonsleder = new Operasjonsleder(monitorUkryptert, antTelegrafister);
        Telegrafist[] telegrafister = lagTelegrafister(antTelegrafister, monitorKryptert);
        Kryptograf[] kryptografer = lagKryptografer(antKryptografer, monitorKryptert, monitorUkryptert);

        // Oppretter og starter Threads
        for(Telegrafist t : telegrafister) new Thread(t, "telegraf").start();
        for(Kryptograf k : kryptografer) new Thread(k, "kryptograf").start();
        new Thread(operasjonsleder, "operasjonsleder").start();
    }

    private static Telegrafist[] lagTelegrafister(int antall, Monitor monitorKryptert){
        Telegrafist[] telegrafister = new Telegrafist[antall];
        for (int i = 0; i < antall; i++) {
            telegrafister[i] = new Telegrafist(kanaler[i], monitorKryptert);
        }
        return telegrafister;
    }

    private static Kryptograf[] lagKryptografer(int antall, Monitor monitorKryptert, Monitor monitorUkryptert){
        Kryptograf[] kryptografer = new Kryptograf[antall];
        for (int i = 0; i < antall; i++) {
            kryptografer[i] = new Kryptograf(monitorKryptert, monitorUkryptert);
        }
        return kryptografer;
    }

}