import java.util.ArrayList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class Monitor {

    private ArrayList<Melding> meldinger = new ArrayList<>();
    private Lock lås = new ReentrantLock();
    private Condition nyeMeldinger = lås.newCondition();

    private boolean ferdig = false;
    private int innsendere;

    Monitor(int innsendere){
        this.innsendere = innsendere;
    }

    void nyMelding(Melding melding) {
        lås.lock();
        try {
            if(melding != null){
                meldinger.add(melding);
                nyeMeldinger.signalAll();
            } else {
                innsendere--;
                if(innsendere == 0){
                    ferdig = true;
                    nyeMeldinger.signalAll();
                }
            }
        } finally {
            lås.unlock();
        }
    }

    Melding hentMelding() throws InterruptedException {
        lås.lock();
        try {
            while(meldinger.isEmpty()){
                if(ferdig) return null;
                nyeMeldinger.await();
            }
            return meldinger.remove(0);
        }
        finally {
            lås.unlock();
        }
    }


}