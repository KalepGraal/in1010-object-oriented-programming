public class Melding implements Comparable<Melding>{
    private String melding;
    private int sekvensnummer;
    private int kanalId;

    Melding(String melding, int sekvensnummer, int kanalId){
        this.melding = melding;
        this.sekvensnummer = sekvensnummer;
        this.kanalId = kanalId;
    }

    @Override
    public int compareTo(Melding mld) {
        return this.sekvensnummer - mld.getSekvensnummer();

    }

    String getMelding(){
        return melding;
    }

    void settMelding(String nyMelding){
        melding = nyMelding;
    }

    private int getSekvensnummer(){
        return sekvensnummer;
    }

    int getKanalId(){
        return kanalId;
    }
}