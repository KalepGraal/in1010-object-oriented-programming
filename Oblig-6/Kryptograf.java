public class Kryptograf implements Runnable{

    private Monitor monitorKryptert;
    private Monitor monitorUkryptert;

    Kryptograf(Monitor monitorKryptert, Monitor monitorUkryptert){
        this.monitorKryptert = monitorKryptert;
        this.monitorUkryptert = monitorUkryptert;
    }

    @Override
    public void run() {
        try {
            Melding melding = monitorKryptert.hentMelding();
            while(melding != null){
                dekrypter(melding);
                monitorUkryptert.nyMelding(melding);
                melding = monitorKryptert.hentMelding();
            }
        } catch (InterruptedException ex){
            ex.printStackTrace();
        }
        monitorUkryptert.nyMelding(null);
    }

    private void dekrypter(Melding mld){
        mld.settMelding(Kryptografi.dekrypter(mld.getMelding()));
    }
}